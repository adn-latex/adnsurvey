# Templates for Surveys

## Table-like Survey (`adnsruvey.cls`)

The class `adnsurvey.cls` it is build so it can typeset and create several surveys. The survey works through the use of a form (using [`auto multiple choice`](http://home.gna.org/auto-qcm/index.en)).

The survey allows the use of questions withing the `\onecopy` macro or following the automatic implementation (see `\questions` definition in [How to define questions](#how-to-define-questions))

In general, elements inside the questions (`\question`) can be any element that can be interpreted by `adnsurvey.cls` and defined by [`pgfkeys`](http://mirrors.ctan.org/graphics/pgf/base/doc/pgfmanual.pdf).

### How to Define Questions

To define a new survey we need to define several elements (similar to the contents of `questions.tex`), like

```tex
\def\questions{%
  element,
  {
    sub element of list,
    sub element of list,
    ...
    sub element of list
  },
  ...
  element
}
```

The elements can be simple or a set (grouped by curly brackets: `{` and `}`).

The current elements that are implemented in `adnsurvey.cls` are:

* Questions. The questions are defined through `question={<id>}{<text>}`. Note that questions need a valid `LaTeX` identifier. The questions must have a type defined by `type=<type>`. By default, the questions have the type set to multiple choice (`oneitem`). However, they can change type by setting options within a group.
  
  * Multiple choice: defined by `type=oneitem`.
  * Open: defined by `type=openitem`.
  * Question's options. Additionally to the type of the question, each question can have several options defined through `options={<option 1>, ..., <option N>}`. For example, one can establish parameters for the open questions (following the defined options in AMC): `options={lines=3, dots=true, lineheight=.3cm}`. 
* Sections. The survey can have headers (or sections) with an specific style. They are defined by `section=<text>`.
* Text. It can be added text to the survey (such as instructions or comments). This type of element is defined by `text=<text>`.
* Header options. It can be set a header to the table (above any row actually) to name the options that are present in the survey. This can show the current options if it is executed without any options or add text to the row in which is typeset: `header[=<text>]` (`=<text>` is optional).
* Commands. One can execute `LaTeX` macros as the list is parsed, through `exec=<cmd>`. This is useful if one can create a new page or call some other macro.
* Automatic header. One can control the default header that appears in each page with the options `auto header on` y `auto header off`. Each is processed as an element in the list of questions.

